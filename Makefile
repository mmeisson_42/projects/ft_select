# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/14 21:39:23 by mmeisson          #+#    #+#              #
#    Updated: 2016/03/18 14:07:25 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = clang

NAME = ft_select

SRC_PATH = ./src/
SRC_NAME = ft_actions.c ft_curs.c ft_display.c ft_error.c ft_fct.c ft_color.c\
		   ft_get_sel.c ft_hook.c ft_init.c ft_putspecs.c ft_rputchar.c\
		   ft_sel.c ft_select.c ft_send.c ft_shutdown.c ft_resize.c\
		   ft_signal_manager.c ft_preload.c ft_reinit.c ft_manage_start.c\
		   ft_search.c

OBJ_PATH = ./obj/
OBJ_NAME = $(SRC_NAME:.c=.o)

INC_PATH = ./inc 

LIB_PATH = ./libft/

CFLAGS = -Wall -Werror -Wextra

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
LIB = $(addprefix -L,$(LIB_PATH))
INC = $(addprefix -I,$(INC_PATH))

LIB_NAMES = -lft -ltermcap
LDFLAGS = $(LIB) $(LIB_NAMES)


all: $(NAME)

$(NAME): $(OBJ)
	make -C $(LIB_PATH)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH) 2> /dev/null
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean:
	@rm -rf $(OBJ_PATH)

fclean: clean
	@rm -f $(NAME)

fcleanall: clean
	make -C $(LIB_PATH) fclean
	@rm -f $(NAME)

re: fclean all
