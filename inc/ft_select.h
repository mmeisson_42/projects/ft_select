/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 21:43:35 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/26 14:23:46 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include "libft.h"
# include <sys/stat.h>
# include <termios.h>
# include <termcap.h>
# include <fcntl.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <signal.h>
# include <sys/ioctl.h>

# define COL_SIZE 6
# define DEF  "\x1B[0m"
# define RED  "\x1B[91m"
# define GRN  "\x1B[92m"
# define YEL  "\x1B[93m"
# define BLU  "\x1B[94m"
# define MAG  "\x1B[95m"
# define CYN  "\x1B[96m"

# define KEYUP "\x1b\x5b\x41"
# define KEYDOWN "\x1b\x5b\x42"
# define KEYRIGHT "\x1b\x5b\x43"
# define KEYLEFT "\x1b\x5b\x44"
# define SPACE " "
# define ENTER "\n"
# define DEL "\x7f"
# define DELETE "\x1b\x5b\x33"
# define ESC "\x1b"

typedef struct		s_sel
{
	struct s_sel	*next;
	struct s_sel	*prev;
	char			*str;
	int				col_size;
	char			color[8];
	char			cursor;
	char			selected;
}					t_sel;

typedef struct		s_coord
{
	int			x;
	int			y;
}					t_coord;

typedef struct		s_data
{
	t_sel				*list;
	t_sel				***array;
	int					fd;
	int					nb_elems;
	int					f_index;
	int					l_index;
	int					real_size;
	int					last_y;
	struct termios		original_term;
	t_coord				c_cursor;
	t_sel				*s_cursor;
	t_coord				size;
	t_coord				winsize;
}					t_data;

typedef struct		s_keyfct
{
	char	*key;
	void	(*fct)(t_data*);
}					t_keyfct;

void				ft_init(t_data *d, int nb_elems);
void				ft_display(t_data *d);
void				ft_hook(t_data *d);
void				ft_send(t_data *d);
void				ft_signal_handler(void);
void				ft_set_termios(t_data *d);

t_sel				*ft_get_sel(int nb_elems, char **elems, t_data *d);
t_sel				*ft_selnew(char *str, t_data *d);
void				ft_seladd(t_sel **list, t_sel *node);
void				ft_seldel(t_sel **node);

void				ft_get_winsize(t_coord *c);
int					ft_get_real_size(t_sel *list);
int					ft_rputchar(int c);
void				ft_putspecs(t_sel *s, t_data *d);
void				ft_set_arrays(t_data *d);

void				ft_curs_up(t_data *d);
void				ft_curs_down(t_data *d);
void				ft_curs_right(t_data *d);
void				ft_curs_left(t_data *d);
void				ft_curs_move(t_data *d);

void				ft_shutdown(t_data *d);
void				ft_select(t_data *d);
void				ft_delete(t_data *d);

void				ft_malloc_error(t_data *d);
void				ft_term_error(int err, t_data *d);

void				ft_resize(void);
void				ft_manage_start(t_data *d);
void				ft_reinit(t_data *d);
void				ft_signal_manager(int sig);
void				ft_preload(t_data *d);
void				ft_colorize(t_sel *s);

t_sel				*ft_sel_src(t_sel *list, char *str, int size);
void				ft_get_coords(t_data *d);

#endif
