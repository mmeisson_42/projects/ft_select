/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strover.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 20:56:24 by mmeisson          #+#    #+#             */
/*   Updated: 2015/11/29 15:46:21 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strover(char *erase, const char *str)
{
	char	*n_str;
	int		i;
	int		j;

	i = (erase) ? ft_strlen(erase) : 0;
	j = (str) ? ft_strlen(str) : 0;
	n_str = ft_memalloc(sizeof(char) * (i + j + 1));
	if (n_str)
	{
		if (erase)
			ft_strcpy(n_str, erase);
		if (str)
			ft_strcat(n_str, str);
		if (erase)
			free(erase);
	}
	return (n_str);
}
