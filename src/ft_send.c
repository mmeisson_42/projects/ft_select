/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_send.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 01:10:11 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/18 14:01:59 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void		ft_send(t_data *d)
{
	int			tem;

	tem = 0;
	tputs(tgetstr("ho", NULL), 1, ft_rputchar);
	tputs(tgetstr("cd", NULL), 1, ft_rputchar);
	tputs(tgetstr("cl", NULL), 1, ft_rputchar);
	tputs(tgetstr("te", NULL), 1, ft_rputchar);
	tputs(tgetstr("ve", NULL), 1, ft_rputchar);
	d->list->prev->next = NULL;
	while (d->list)
	{
		if (d->list->selected)
		{
			ft_putstr(d->list->str);
			ft_putchar(' ');
			tem = 1;
		}
		d->list = d->list->next;
	}
	if (tem)
		ft_putchar('\n');
	tcsetattr(0, TCSADRAIN, &(d->original_term));
	close(d->fd);
	exit(EXIT_SUCCESS);
}
