/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 21:57:41 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/18 13:46:17 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void		ft_malloc_error(t_data *d)
{
	if (d)
		;
	ft_putendl_fd("Fatal Error : Can't allocate memory", 2);
	ft_shutdown(NULL);
}

void		ft_term_error(int err, t_data *d)
{
	const char	*(str[3]) = {
		"Fatal Error: TERM variable not set",
		"Fatal Error: Terminfo database not found",
		"Fatal Error: Term attributes not found",
	};

	if (d)
		;
	ft_putendl_fd(str[err], 2);
	exit(EXIT_FAILURE);
}
