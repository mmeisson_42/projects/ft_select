/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rputchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 23:32:27 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/22 19:50:49 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

int			ft_rputchar(int c)
{
	static int		fd = -1;

	if (c == -1)
	{
		if (fd != -1)
		{
			close(fd);
			fd = -1;
		}
	}
	else if (fd == -1)
		fd = open(ttyname(0), O_RDWR);
	else
		write(2, (char*)&c, 1);
	return (c);
}
