/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_sel.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 22:03:48 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/17 21:19:47 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

t_sel			*ft_get_sel(int nb_elems, char **elems, t_data *d)
{
	int		i;
	t_sel	*node;
	t_sel	*list;

	i = 0;
	list = NULL;
	while (i < nb_elems)
	{
		node = ft_selnew(elems[i], d);
		ft_seladd(&list, node);
		ft_colorize(node);
		i++;
	}
	return (list);
}
