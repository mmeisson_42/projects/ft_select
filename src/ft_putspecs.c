/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putspecs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 23:48:12 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/22 18:53:38 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void		ft_fill_spaces(char *str, t_data *d)
{
	size_t		i;

	i = d->real_size - ft_strlen(str);
	while (i--)
		ft_putchar_fd(' ', d->fd);
}

void			ft_putspecs(t_sel *s, t_data *d)
{
	if (s->cursor)
		tputs(tgetstr("us", NULL), 1, ft_rputchar);
	if (s->selected)
		tputs(tgetstr("mr", NULL), 1, ft_rputchar);
	if (s->color[0])
		ft_putstr_fd(s->color, d->fd);
	ft_putstr_fd(s->str, d->fd);
	if (s->color[0])
		ft_putstr_fd(DEF, d->fd);
	if (s->cursor)
		tputs(tgetstr("ue", NULL), 1, ft_rputchar);
	if (s->selected)
		tputs(tgetstr("me", NULL), 1, ft_rputchar);
	if (d->size.x > 1)
		ft_fill_spaces(s->str, d);
}
