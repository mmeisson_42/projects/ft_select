/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_curs.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 00:25:06 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/22 19:50:47 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void		ft_curs_move(t_data *d)
{
	d->s_cursor->cursor = 0;
	d->s_cursor = d->array[d->c_cursor.x][d->c_cursor.y];
	d->s_cursor->cursor = 1;
}

void		ft_curs_up(t_data *d)
{
	if (d->c_cursor.y > 0)
		d->c_cursor.y--;
	else
	{
		if (d->c_cursor.x == 0)
		{
			d->c_cursor.y = d->last_y;
			d->c_cursor.x = d->size.x - 1;
		}
		else
		{
			d->c_cursor.x--;
			d->c_cursor.y = d->size.y - 1;
		}
	}
	ft_curs_move(d);
}

void		ft_curs_down(t_data *d)
{
	if (d->c_cursor.x == d->size.x - 1)
	{
		if (d->c_cursor.y == d->last_y)
		{
			d->c_cursor.x = 0;
			d->c_cursor.y = 0;
		}
		else
			d->c_cursor.y++;
	}
	else
	{
		if (d->c_cursor.y < d->size.y - 1)
			d->c_cursor.y++;
		else
		{
			d->c_cursor.y = 0;
			d->c_cursor.x++;
		}
	}
	ft_curs_move(d);
}

void		ft_curs_right(t_data *d)
{
	if (d->c_cursor.x < d->size.x - 1)
	{
		d->c_cursor.x++;
		if (d->c_cursor.x == d->size.x - 1 && d->c_cursor.y > d->last_y)
			d->c_cursor.y = d->last_y;
	}
	else
		d->c_cursor.x = 0;
	ft_curs_move(d);
}

void		ft_curs_left(t_data *d)
{
	if (d->c_cursor.x > 0)
		d->c_cursor.x--;
	else
	{
		if (d->c_cursor.y > d->last_y)
			d->c_cursor.x = d->size.x - 2;
		else
			d->c_cursor.x = d->size.x - 1;
	}
	ft_curs_move(d);
}
