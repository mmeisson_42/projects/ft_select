/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal_manager.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 13:09:40 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/26 14:23:44 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void		ft_signal_manager(int sig)
{
	if (sig == 28)
		ft_resize();
	else if (sig == 19)
	{
		ft_set_termios(NULL);
		ft_reinit(NULL);
		ft_display(NULL);
	}
	else
		ft_shutdown(NULL);
}

void		ft_signal_handler(void)
{
	const int		tab[] = {1, 2, 9, 13, 14, 15, 19, 24, 25, 26, 27, 28, 0};
	int				i;

	i = 0;
	while (tab[i] != 0)
	{
		signal(tab[i], ft_signal_manager);
		i++;
	}
}
