/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 22:33:17 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/23 15:28:07 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void			ft_set_termios(t_data *d)
{
	char			*name;
	struct termios	t;
	static t_data	*s_d = NULL;

	if (d)
		s_d = d;
	else
	{
		if (!(name = getenv("TERM")))
			ft_term_error(0, s_d);
		else if (tgetent(NULL, name) < 1)
			ft_term_error(1, s_d);
		else if (tcgetattr(0, &t) == -1)
			ft_term_error(1, s_d);
		ft_memcpy(&(s_d->original_term), &t, sizeof(struct termios));
		t.c_lflag &= ~(ICANON | ECHO);
		t.c_cc[VMIN] = 1;
		t.c_cc[VTIME] = 0;
		tcsetattr(0, TCSADRAIN, &t);
		tputs(tgetstr("ti", NULL), 1, ft_rputchar);
		tputs(tgetstr("vi", NULL), 1, ft_rputchar);
	}
}

void			ft_init(t_data *d, int nb_elems)
{
	d->fd = open(ttyname(0), O_RDWR);
	ft_set_termios(NULL);
	ft_get_winsize(&(d->winsize));
	d->nb_elems = nb_elems;
	d->size.y = (nb_elems > d->winsize.y) ? d->winsize.y : nb_elems;
	d->size.x = nb_elems / d->size.y;
	if (nb_elems % d->size.y)
		d->size.x++;
	d->c_cursor.x = 0;
	d->c_cursor.y = 0;
	d->s_cursor = d->list;
	d->s_cursor->cursor = 1;
	d->array = NULL;
	d->real_size = ft_get_real_size(d->list);
	d->last_y = (d->nb_elems % d->size.y) ? d->nb_elems % d->size.y - 1 :
		d->size.y - 1;
	d->f_index = 0;
	d->l_index = (d->winsize.x / d->real_size) - (d->f_index);
	ft_set_arrays(d);
}
