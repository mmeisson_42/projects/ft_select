/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_search.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/17 21:28:00 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/17 22:29:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

t_sel		*ft_sel_src(t_sel *list, char *str, int size)
{
	t_sel		*tmp;

	tmp = list;
	while (42)
	{
		if (ft_strnequ(str, tmp->str, size))
			return (tmp);
		tmp = tmp->next;
		if (tmp == list)
			return (NULL);
	}
}
