/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sel.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 21:57:51 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/18 12:38:20 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

t_sel		*ft_selnew(char *str, t_data *d)
{
	t_sel		*node;

	if (!(node = malloc(sizeof(t_sel))))
		ft_malloc_error(d);
	node->next = NULL;
	node->prev = NULL;
	ft_bzero(node->color, sizeof(char) * 4);
	node->str = str;
	node->col_size = (ft_strlen(str) / COL_SIZE) + 1;
	node->cursor = 0;
	node->selected = 0;
	return (node);
}

void		ft_seladd(t_sel **list, t_sel *node)
{
	if (!*list)
	{
		*list = node;
		node->next = node;
		node->prev = node;
	}
	else
	{
		node->prev = (*list)->prev;
		node->next = *list;
		(*list)->prev = node;
		node->prev->next = node;
	}
}

void		ft_seldel(t_sel **node)
{
	t_sel		*tmp;

	if (*node)
	{
		tmp = *node;
		tmp->prev->next = tmp->next;
		tmp->next->prev = tmp->prev;
		free(tmp);
	}
}
