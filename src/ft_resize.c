/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_resize.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 12:16:50 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/17 22:54:17 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"
#include <time.h>

void		ft_resize(void)
{
	tputs(tgetstr("ho", NULL), 1, ft_rputchar);
	tputs(tgetstr("cd", NULL), 1, ft_rputchar);
	ft_reinit(NULL);
	ft_display(NULL);
}
