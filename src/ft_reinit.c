/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reinit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 20:40:14 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/26 14:32:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void			ft_get_coords(t_data *d)
{
	t_sel		*list;

	list = d->list;
	ft_bzero(&(d->c_cursor), sizeof(t_coord));
	while (!list->cursor)
	{
		d->c_cursor.y++;
		list = list->next;
		if (d->c_cursor.y == d->size.y)
		{
			d->c_cursor.y = 0;
			d->c_cursor.x++;
		}
	}
}

void			ft_reinit(t_data *d)
{
	static t_data		*s_d = NULL;

	if (d)
		s_d = d;
	else
	{
		ft_get_winsize(&(s_d->winsize));
		s_d->size.y = (s_d->nb_elems > s_d->winsize.y) ?
			s_d->winsize.y : s_d->nb_elems;
		s_d->size.x = s_d->nb_elems / s_d->size.y;
		if (s_d->nb_elems % s_d->size.y)
			s_d->size.x++;
		s_d->real_size = ft_get_real_size(s_d->list);
		s_d->last_y = (s_d->nb_elems % s_d->size.y) ? s_d->nb_elems %
			s_d->size.y - 1 : s_d->size.y - 1;
		s_d->f_index = 0;
		s_d->l_index = (s_d->winsize.x / s_d->real_size) - s_d->f_index;
		ft_set_arrays(s_d);
		ft_get_coords(s_d);
	}
}
