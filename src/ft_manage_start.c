/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_manage_start.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/17 14:42:18 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/17 15:40:12 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void			ft_manage_start(t_data *d)
{
	int		max_col;

	max_col = d->winsize.x / d->real_size;
	while (d->c_cursor.x >= d->l_index)
	{
		d->l_index++;
		d->f_index++;
	}
	while (d->c_cursor.x < d->f_index)
	{
		d->l_index--;
		d->f_index--;
	}
}
