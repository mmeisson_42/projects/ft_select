/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 21:58:05 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/17 20:07:32 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void	ft_select(t_data *d)
{
	d->s_cursor->selected = (d->s_cursor->selected) ? 0 : 1;
	ft_curs_down(d);
}

int		main(int ac, char **av)
{
	t_data		d;

	ft_signal_handler();
	if (ac > 1)
		if ((d.list = ft_get_sel(--ac, ++av, &d)))
		{
			ft_preload(&d);
			ft_init(&d, ac);
			ft_display(NULL);
			ft_hook(&d);
		}
	return (EXIT_SUCCESS);
}
