/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fct.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 22:55:49 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/18 13:49:03 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void			ft_get_winsize(t_coord *c)
{
	struct winsize		w;

	ioctl(0, TIOCGWINSZ, &w);
	c->x = w.ws_col;
	c->y = w.ws_row;
}

int				ft_get_real_size(t_sel *list)
{
	int			size;
	t_sel		*tmp;

	if (!list)
		return (0);
	tmp = list->next;
	size = list->col_size;
	while (tmp != list)
	{
		tmp = tmp->next;
		if (tmp->col_size > size)
			size = tmp->col_size;
	}
	return (size * COL_SIZE);
}

void			ft_del_arrays(t_sel ***a)
{
	int		i;

	i = 0;
	if (a)
	{
		while (a[i])
		{
			free(a[i]);
			i++;
		}
		free(a);
	}
}

void			ft_set_arrays(t_data *d)
{
	int		i;
	int		j;
	t_sel	*tmp;
	t_sel	***a;

	a = d->array;
	ft_del_arrays(a);
	if (!(a = ft_memalloc(sizeof(t_sel**) * (d->size.x + 1))))
		ft_malloc_error(d);
	i = -1;
	tmp = d->list;
	while (++i < d->size.x)
	{
		if (!(a[i] = ft_memalloc(sizeof(t_sel*) * (d->size.y + 1))))
			ft_malloc_error(d);
		j = -1;
		while (++j < d->size.y)
		{
			a[i][j] = tmp;
			tmp = tmp->next;
		}
	}
	d->array = a;
}
