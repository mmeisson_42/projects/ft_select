/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_preload.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 13:26:23 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/17 19:58:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void		ft_preload(t_data *d)
{
	ft_rputchar(0);
	ft_shutdown(d);
	ft_reinit(d);
	ft_display(d);
	ft_set_termios(d);
}
