/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 23:38:17 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/18 14:03:26 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void		ft_display(t_data *d)
{
	int				i;
	int				j;
	static t_data	*s_d = NULL;

	if (d)
		s_d = d;
	else
	{
		j = 0;
		ft_manage_start(s_d);
		tputs(tgetstr("ho", NULL), 1, ft_rputchar);
		tputs(tgetstr("cd", NULL), 1, ft_rputchar);
		if (s_d->size.y > 0 && s_d->real_size < s_d->winsize.x)
			while (j < s_d->size.y)
			{
				i = s_d->f_index;
				while (i < s_d->size.x && (i * s_d->size.y) + j <
						(int)s_d->nb_elems && i < s_d->l_index)
					ft_putspecs((s_d->array)[i++][j], s_d);
				j++;
				if (j < s_d->size.y)
					ft_putchar_fd('\n', s_d->fd);
			}
	}
}
