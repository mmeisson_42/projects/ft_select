/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_shutdown.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 01:04:43 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/26 14:09:16 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void		ft_shutdown(t_data *d)
{
	static t_data		*s_d = NULL;

	if (s_d)
	{
		tputs(tgetstr("ho", NULL), 1, ft_rputchar);
		tputs(tgetstr("cd", NULL), 1, ft_rputchar);
		tputs(tgetstr("cl", NULL), 1, ft_rputchar);
		tputs(tgetstr("te", NULL), 1, ft_rputchar);
		tputs(tgetstr("ve", NULL), 1, ft_rputchar);
		if (s_d)
			tcsetattr(0, TCSADRAIN, &(s_d->original_term));
		close(s_d->fd);
		exit(EXIT_SUCCESS);
	}
	else if (d)
		s_d = d;
}
