/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_actions.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 00:46:16 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/18 12:50:31 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void		ft_delete(t_data *d)
{
	t_sel		*tmp;

	if (d->nb_elems == 1)
		ft_shutdown(d);
	tmp = d->s_cursor;
	ft_curs_down(d);
	if (tmp == d->list)
		d->list = d->list->next;
	ft_seldel(&tmp);
	d->nb_elems--;
	ft_reinit(NULL);
}
