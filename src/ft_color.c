/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/17 21:01:38 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/26 14:31:04 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void		ft_colorize(t_sel *s)
{
	struct stat		s_stat;

	if (lstat(s->str, &s_stat) != -1)
	{
		if (S_ISREG(s_stat.st_mode))
			ft_strcpy(s->color, GRN);
		else if (S_ISDIR(s_stat.st_mode))
			ft_strcpy(s->color, RED);
		else if (S_ISBLK(s_stat.st_mode))
			ft_strcpy(s->color, BLU);
		else if (S_ISLNK(s_stat.st_mode))
			ft_strcpy(s->color, YEL);
		else if (S_ISSOCK(s_stat.st_mode))
			ft_strcpy(s->color, MAG);
		else if (S_ISFIFO(s_stat.st_mode))
			ft_strcpy(s->color, CYN);
	}
}
