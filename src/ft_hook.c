/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hook.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 23:56:54 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/18 13:51:42 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void		ft_update_cursor(t_data *d, t_sel *src)
{
	d->s_cursor->cursor = 0;
	src->cursor = 1;
	d->s_cursor = src;
	ft_get_coords(d);
	ft_display(NULL);
}

static void		ft_manage_search(char c, t_data *d)
{
	static char			*search = NULL;
	static int			size = 0;
	t_sel				*src;
	int					tem;

	tem = 0;
	if (!search && !(search = ft_strnew(1024)))
		ft_malloc_error(d);
	if (c != -1 && size < 1024)
	{
		search[size++] = c;
		if ((src = ft_sel_src(d->list, search, size)))
		{
			tem = 1;
			ft_update_cursor(d, src);
		}
	}
	if (!tem)
	{
		ft_strclr(search);
		size = 0;
	}
}

static void		(*ft_get_fct(char *buf))(t_data *d)
{
	const t_keyfct	assoc[10] = {
		{ KEYUP, ft_curs_up },
		{ KEYDOWN, ft_curs_down },
		{ KEYRIGHT, ft_curs_right },
		{ KEYLEFT, ft_curs_left },
		{ SPACE, ft_select },
		{ ESC, ft_shutdown },
		{ DEL, ft_delete },
		{ DELETE, ft_delete },
		{ ENTER, ft_send },
		{ ESC, ft_shutdown },
	};
	int				i;

	i = -1;
	while (++i < 10)
		if (ft_strequ(assoc[i].key, buf))
			return (assoc[i].fct);
	return (NULL);
}

void			ft_hook(t_data *d)
{
	char			buffer[4];
	void			(*fct)(t_data*);

	while (42)
	{
		ft_bzero(buffer, sizeof(char) * 4);
		read(0, buffer, 3);
		if ((fct = ft_get_fct(buffer)))
		{
			ft_manage_search(-1, NULL);
			fct(d);
			ft_display(NULL);
		}
		else
			ft_manage_search(buffer[0], d);
	}
}
